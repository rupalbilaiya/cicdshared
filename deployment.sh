#!/bin/bash

filename=$1

x=0
want=5
cat $filename | while read line; do
  x=$(( x+1 ))
  echo $line
  if [[ $x -gt "$want" ]] && [[ "${line: -4}" == ".sql" ]] && [[ "${line: 7}" != "Update " ]]; then
    echo $line
    echo $db_name
    echo $role_name
    echo $warehouse
    echo ${SNOWSQL_ACCOUNT}
    echo ${SNOWSQL_USER}
    ~/bin/snowsql -a ${SNOWSQL_ACCOUNT} -u ${SNOWSQL_USER} -f $line;
  fi
done

