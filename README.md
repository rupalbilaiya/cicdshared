# CI/CD pipeline in GITLAB

Automate the process of executing merged files to PROD(main/master) or UAT branch to corresponding production and UAT database(s) over Snowflake.

![image.png](./image.png)
## Authors

- [@Rupal Bilaiya](rupal.bilaiya@gmail.com)
- [@Vivek Pratap Singh](vivekprsh@gmail.com)


## Acknowledgements

 - https://medium.com/hackernoon/configuring-gitlab-ci-on-aws-ec2-using-docker-7c359d513a46
## Tech Stack

**Language:** Shell, Snowflake SQL

**Tools:** snowsql, gitlab, ubuntu/linux


## Project Description

Please refer to below document:
https://gitlab.com/rupalbilaiya/cicdshared/-/blob/main/CI_CD_Pipleline.docx
## Documentation

[GITLAB Project](https://gitlab.com/rupalbilaiya/cicdshared)

